import Vue from 'vue'
import App from './App.vue'
import store from './store'
import './../node_modules/uikit/dist/css/uikit.min.css'
import './../node_modules/uikit/dist/js/uikit'
import './../node_modules/uikit/dist/js/uikit-icons'

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
