import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: 'gif-app',
  storage: localStorage
})

const settings = {
  apiKey: 'EwQCHDTYU2onchg4pwYQmRFRcugz5ySa',
  limit: 100,
  offset: 0,
  rating: 'g',
  lang: 'pl'
}

export default new Vuex.Store({
  state: {
    gifSearch: [],
    gifTrend: [],
    gifLiked: [],
    searchString: '',
    searching: false
  },
  mutations: {
    // accessed only through actions
    setSearchString (state, searchTerm) {
      state.searchString = searchTerm
    },
    searchingState (state, bool) {
      state.searching = bool
    },
    clearGifs (state) {
      state.gifSearch = []
    },
    setGifs (state, gifs) {
      state.gifSearch = gifs
    },
    setTrends (state, gifs) {
      state.gifTrend = gifs
    },
    addLike (state, gif) {
      state.gifLiked.push(gif)
    },
    removeLike (state, gif) {
      state.gifLiked.splice(state.gifLiked.indexOf(gif), 1)
    }
  },
  actions: {
    getSearch (state, searchQuery) {
      console.log('Searching Query...', settings)
      state.commit('searchingState', true)
      state.commit('setSearchString', searchQuery)
      Axios.get('http://api.giphy.com/v1/gifs/search', {
        params: {
          q: searchQuery,
          apiKey: settings.apiKey,
          limit: settings.limit,
          rating: settings.rating,
          offset: settings.offset
        }
      })
        .then((res) => {
          state.commit('setGifs', res.data.data)
          state.commit('searchingState', false)
        })
      return true
    },
    getTrends (state) {
      console.log('Searching Trends...', settings)
      Axios.get('http://api.giphy.com/v1/gifs/trending', {
        params: {
          apiKey: settings.apiKey,
          limit: settings.limit,
          rating: settings.rating,
          offset: settings.offset
        }
      })
        .then((res) => {
          state.commit('setTrends', res.data.data)
        })
      return true
    },
    onLike (state, gif) {
      var likz = state.getters.gimmeLikedList
      var isAlreadyLiked = likz.some((gifToInspect) => {
        return gifToInspect.id === gif.id
      })
      if (!isAlreadyLiked) {
      // if (likz.indexOf(gif) < 0) {
        state.commit('addLike', gif)
        return true
      } else {
        window.alert('Gif już polubiony!')
        return false
      }
    },
    onUnlike (state, gif) {
      state.commit('removeLike', gif)
    },
    onStartClearString (state) {
      state.commit('setSearchString', '')
    }
  },
  getters: {
    isSearching (state) {
      return state.searching
    },
    gimmeSearchString (state) {
      return state.searchString
    },
    gimmeTrendList (state) {
      return state.gifTrend
    },
    gimmeSearchedList (state) {
      return state.gifSearch
    },
    gimmeLikedList (state) {
      return state.gifLiked
    }
  },
  plugins: [vuexPersist.plugin]
})
